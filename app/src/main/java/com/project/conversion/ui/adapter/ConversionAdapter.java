package com.project.conversion.ui.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.conversion.R;
import com.project.conversion.data.model.Unit;

import java.util.ArrayList;

import javax.inject.Inject;

public class ConversionAdapter extends RecyclerView.Adapter<ConversionAdapter.UnitViewHolder> {

    private ArrayList<Unit> unitDataList;

    @Inject
    public ConversionAdapter() {
        unitDataList = new ArrayList<>();
    }

    public void setUnitDataList(ArrayList<Unit> unitDataList) {
        this.unitDataList = unitDataList;
    }

    @Override
    public int getItemCount() {
        return unitDataList.size();
    }

    @NonNull
    @Override
    public UnitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_unit, null);
        return new UnitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UnitViewHolder holder, int position) {
        holder.bind(unitDataList.get(position));
    }

    static class UnitViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCelsius, tvFaren;

        UnitViewHolder(View itemView) {
            super(itemView);
            tvCelsius = itemView.findViewById(R.id.tv_celsius);
            tvFaren = itemView.findViewById(R.id.tv_faren);

        }

        @SuppressLint("SetTextI18n")
        void bind(Unit data) {
            tvCelsius.setText("Celius: " + data.getCelsius());
            if (data.getFarenheit() != null)
                tvFaren.setText("Farenheit: " + data.getFarenheit());
            else
                tvFaren.setText("Farenheit: -");

        }
    }
}
