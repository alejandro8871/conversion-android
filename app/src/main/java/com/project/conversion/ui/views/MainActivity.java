package com.project.conversion.ui.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.project.conversion.ConversionApp;
import com.project.conversion.R;
import com.project.conversion.data.model.Unit;
import com.project.conversion.injection.unit.DaggerUnitComponent;
import com.project.conversion.injection.unit.UnitComponent;
import com.project.conversion.injection.unit.UnitModule;
import com.project.conversion.presenters.UnitPresenter;
import com.project.conversion.ui.adapter.ConversionAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements UnitView {

    @Inject
    UnitPresenter presenter;

    @Inject
    ConversionAdapter adapter;
    private ProgressDialog progressDialog;
    private EditText etUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_main);
        initInject();
        presenter.init(this);
        RecyclerView rvUnits = findViewById(R.id.rv_units);
        rvUnits.setHasFixedSize(true);
        rvUnits.setLayoutManager(new LinearLayoutManager(this));
        adapter.setUnitDataList(presenter.getArrayUnit());
        rvUnits.setAdapter(adapter);
        etUnit = findViewById(R.id.et_unit_name);
        (findViewById(R.id.btn_conversion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideKeyboard();
                Unit unit = new Unit();
                unit.setCelsius(etUnit.getText().toString());
                presenter.makeTheConversion(unit);
            }

        });
    }

    @Override
    public void setUnits(ArrayList<Unit> list) {
        adapter.setUnitDataList(null);
        adapter.notifyDataSetChanged();
        adapter.setUnitDataList(list);
        adapter.notifyDataSetChanged();
        etUnit.setText("");
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, R.string.enter_unit, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.request_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWaitingDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle(getString(R.string.dialog_title));
            progressDialog.setMessage(getString(R.string.dialog_message));
        }
        progressDialog.show();
    }

    @Override
    public void hideWaitingDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }


    private void initInject() {
        UnitComponent unitComponent = DaggerUnitComponent.builder()
                .applicationComponent(((ConversionApp) getApplication()).getComponent())
                .unitModule(new UnitModule(this))
                .build();
        unitComponent.inject(this);
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
