
package com.project.conversion.ui.views;

import com.project.conversion.data.model.Unit;

import java.util.ArrayList;

public interface UnitView {

    void setUnits(ArrayList<Unit> list);

    void showWaitingDialog();

    void hideWaitingDialog();

    void showErrorMessage();

    void showError();

}
