package com.project.conversion.presenters;

import android.content.Context;

import com.project.conversion.data.model.Unit;

import java.util.ArrayList;

public interface UnitPresenter {

    void makeTheConversion(Unit unit);

    void init(Context context);

    ArrayList<Unit> getArrayUnit();
}
