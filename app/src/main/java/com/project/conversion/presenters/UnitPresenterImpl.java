package com.project.conversion.presenters;

import android.content.Context;

import com.project.conversion.data.model.Unit;
import com.project.conversion.data.sqlite.SqliteController;
import com.project.conversion.domain.conversion.ConversionUnitInteractor;
import com.project.conversion.ui.views.UnitView;

import java.util.ArrayList;

import javax.inject.Inject;

public class UnitPresenterImpl implements UnitPresenter, ConversionUnitInteractor.Callback {

    @Inject
    UnitView view;

    @Inject
    ConversionUnitInteractor conversionUnitInteractor;
    private Context context;
    private SqliteController sqliteController;


    @Inject
    UnitPresenterImpl() {

    }

    @Override
    public void init(Context context) {
        this.context = context;
        sqliteController = new SqliteController(context);
    }

    @Override
    public ArrayList<Unit> getArrayUnit() {
        return sqliteController.getUserDetails();
    }

    @Override
    public void makeTheConversion(Unit unit) {
        if (unit == null || unit.getCelsius().isEmpty()) {
            view.showErrorMessage();
        } else {
            view.showWaitingDialog();
            conversionUnitInteractor.execute(sqliteController, unit, this);

        }
    }


    @Override
    public void onSuccess(ArrayList<Unit> Arraylist) {
        view.hideWaitingDialog();
        view.setUnits(Arraylist);
    }

    @Override
    public void onError(ArrayList<Unit> Arraylist) {
        view.hideWaitingDialog();
        view.setUnits(Arraylist);
        view.showError();
    }
}
