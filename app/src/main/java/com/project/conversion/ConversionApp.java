package com.project.conversion;

import android.app.Application;

import com.project.conversion.injection.application.ApplicationComponent;
import com.project.conversion.injection.application.ApplicationModule;
import com.project.conversion.injection.application.DaggerApplicationComponent;

@SuppressWarnings("deprecation")
public class ConversionApp extends Application {
    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
