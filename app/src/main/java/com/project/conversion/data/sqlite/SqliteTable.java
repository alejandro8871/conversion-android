package com.project.conversion.data.sqlite;

@SuppressWarnings("WeakerAccess")
public class SqliteTable {
    public static final String TABLE_CONVERSION = "table_conversion";
    public static final String COL_USER_ID = " col_user_id";
    public static final String COL_USER_CELSIUS = " col_user_celsius";
    public static final String COL_USER_FARENHEIT = " col_user_farenheit";

    public static final String DB_USER = "CREATE TABLE " + TABLE_CONVERSION + "(" + COL_USER_ID + " INTEGER PRIMARY KEY, "
            + COL_USER_CELSIUS + " TEXT, " + COL_USER_FARENHEIT + " TEXT" + ")";


}
