package com.project.conversion.data.model;

public class Unit {

    private int user_id;
    private String celsius;
    private String farenheit;


    public Unit() {

    }

    public Unit(int id, String celsius, String farenheit) {
        this.user_id = id;
        this.celsius = celsius;
        this.farenheit = farenheit;

    }

    public Unit(String celsius, String farenheit) {

        this.celsius = celsius;
        this.farenheit = farenheit;

    }


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCelsius() {
        return celsius;
    }

    public void setCelsius(String celsius) {
        this.celsius = celsius;
    }

    public String getFarenheit() {
        return farenheit;
    }

    public void setFarenheit(String farenheit) {
        this.farenheit = farenheit;
    }
}
