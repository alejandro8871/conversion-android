package com.project.conversion.data.sqlite;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.project.conversion.data.model.Unit;

import java.util.ArrayList;

@SuppressWarnings("WeakerAccess")
public class SqliteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "CONVERSION_DATABASE";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = SqliteHelper.class.getSimpleName();

    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SqliteTable.DB_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(String.format("DROP TABLE IF EXISTS %s", SqliteTable.DB_USER));
        onCreate(db);
    }

    public boolean insertData(String table, ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.insert(table, null, values);
        if (result == -1) {
            Log.d(TAG, "failed to save data!");
            return false;
        } else {
            Log.d(TAG, "save data successful");
            return true;
        }
    }

    @SuppressLint("Recycle")
    public ArrayList<Unit> getUnits() {
        ArrayList<Unit> users = new ArrayList<>();
        Unit unit;
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {
                SqliteTable.COL_USER_ID,
                SqliteTable.COL_USER_CELSIUS,
                SqliteTable.COL_USER_FARENHEIT
        };

        Cursor cursor = db.query(SqliteTable.TABLE_CONVERSION, columns,
                null, null, null, null, SqliteTable.COL_USER_ID + " DESC");
        if (cursor.moveToFirst()) {
            do {
                unit = new Unit();
                unit.setUser_id(cursor.getInt(0));
                unit.setCelsius(cursor.getString(1));
                unit.setFarenheit(cursor.getString(2));
                users.add(unit);
            } while (cursor.moveToNext());
        }
        return users;
    }

    public boolean deleteUnit(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(SqliteTable.TABLE_CONVERSION, SqliteTable.COL_USER_ID + " =? ",
                new String[]{String.valueOf(id)}) > 0;
    }

}
