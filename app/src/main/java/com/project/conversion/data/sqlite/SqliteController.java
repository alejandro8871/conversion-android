package com.project.conversion.data.sqlite;

import android.content.ContentValues;
import android.content.Context;

import com.project.conversion.data.model.Unit;

import java.util.ArrayList;

@SuppressWarnings("WeakerAccess")
public class SqliteController {

    private SqliteHelper sqliteHelper;
    Context context;

    public SqliteController(Context mContext) {
        this.context = mContext;
        sqliteHelper = new SqliteHelper(context);
    }

    public void saveUserData(Unit user) {
        ContentValues values = new ContentValues();
        values.put(SqliteTable.COL_USER_CELSIUS, user.getCelsius());
        values.put(SqliteTable.COL_USER_FARENHEIT, user.getFarenheit());
        sqliteHelper.insertData(SqliteTable.TABLE_CONVERSION, values);
    }

    public ArrayList<Unit> getUserDetails() {
        return sqliteHelper.getUnits();
    }

    public boolean deleteUnit(int id) {
        return sqliteHelper.deleteUnit(id);
    }

}
