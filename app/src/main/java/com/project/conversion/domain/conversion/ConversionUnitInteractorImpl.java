package com.project.conversion.domain.conversion;


import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.project.conversion.data.model.Unit;
import com.project.conversion.data.sqlite.SqliteController;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

@SuppressWarnings("FieldCanBeLocal")
public class ConversionUnitInteractorImpl implements ConversionUnitInteractor {
    private static final String NAMESPACE = "http://www.w3schools.com/webservices/";
    private static final String URL = "http://www.w3schools.com/webservices/tempconvert.asmx";
    private static final String METHOD_NAME = "CelsiusToFahrenheit";
    private static final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
    private SqliteController sqliteController;
    private Callback callback;
    private Unit unit;
    //TODO Remove if the ws its work
    private boolean isWsWork = false;

    @Inject
    ConversionUnitInteractorImpl() {
    }

    @Override
    public void execute(SqliteController sqliteController, Unit unit, Callback callback) {
        this.unit = unit;
        this.callback = callback;
        this.sqliteController = sqliteController;
        WSTask wsTask = new WSTask();
        wsTask.execute(unit.getCelsius());
    }

    @SuppressLint("StaticFieldLeak")
    private class WSTask extends AsyncTask<String, Integer, Boolean> {

        @SuppressLint("DefaultLocale")
        protected Boolean doInBackground(String... params) {
            boolean resul = true;
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("Celsius", unit.getCelsius());

            SoapSerializationEnvelope envelope =
                    new SoapSerializationEnvelope(SoapEnvelope.VER11);

            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try {
                String res;
                if (isWsWork) {
                    transporte.call(SOAP_ACTION, envelope);
                    SoapPrimitive resultado_xml = (SoapPrimitive) envelope.getResponse();
                    res = resultado_xml.toString();
                } else {
                    final int num = Integer.parseInt(unit.getCelsius());
                    res = String.format("%.2f", convertCelciusToFahrenheit(num));
                }
                unit.setFarenheit(res);
            } catch (SoapFault e) {
                resul = false;
            } catch (HttpResponseException e) {
                if (e.getStatusCode() == 301) {
                    resul = false;
                } else
                    resul = false;
            } catch (IOException e) {
                resul = false;
            } catch (XmlPullParserException e) {
                resul = false;
            }
            return resul;
        }

        protected void onPostExecute(Boolean result) {
            sqliteController.saveUserData(unit);
            if (result)
                callback.onSuccess(getArrayUnit());
            else
                callback.onError(getArrayUnit());
        }
    }

    private float convertCelciusToFahrenheit(float celsius) {
        return ((celsius * 9) / 5) + 32;
    }

    @Override
    public ArrayList<Unit> getArrayUnit() {
        if (sqliteController != null) {
            return sqliteController.getUserDetails();
        }
        return null;
    }
}
