package com.project.conversion.domain.conversion;

import com.project.conversion.data.model.Unit;
import com.project.conversion.data.sqlite.SqliteController;
import com.project.conversion.domain.Interactor;

import java.util.ArrayList;

public interface ConversionUnitInteractor extends Interactor {

    interface Callback {

        void onSuccess(ArrayList<Unit> unit);

        void onError(ArrayList<Unit> unit);
    }

    ArrayList<Unit> getArrayUnit();

    void execute(SqliteController context, Unit unitName, Callback callback);
}
