package com.project.conversion.injection.unit;

import com.project.conversion.domain.conversion.ConversionUnitInteractor;
import com.project.conversion.domain.conversion.ConversionUnitInteractorImpl;
import com.project.conversion.injection.ActivityScope;
import com.project.conversion.presenters.UnitPresenter;
import com.project.conversion.presenters.UnitPresenterImpl;
import com.project.conversion.ui.views.MainActivity;
import com.project.conversion.ui.views.UnitView;

import dagger.Module;
import dagger.Provides;

@Module
public class UnitModule {

    private MainActivity activity;

    public UnitModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public UnitView providesView() {
        return activity;
    }

    @Provides
    @ActivityScope
    public UnitPresenter providesPresenter(UnitPresenterImpl unitPresenter) {
        return unitPresenter;
    }

    @Provides
    @ActivityScope
    public ConversionUnitInteractor providesInteractor(ConversionUnitInteractorImpl interactor) {
        return interactor;
    }
}
