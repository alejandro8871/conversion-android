package com.project.conversion.injection.unit;

import com.project.conversion.domain.conversion.ConversionUnitInteractor;
import com.project.conversion.injection.ActivityScope;
import com.project.conversion.injection.application.ApplicationComponent;
import com.project.conversion.presenters.UnitPresenter;
import com.project.conversion.ui.views.MainActivity;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = UnitModule.class
)
public interface UnitComponent {

    void inject(MainActivity activity);

    UnitPresenter providesPresenter();

    ConversionUnitInteractor providesInteractor();
}
