package com.project.conversion.injection.application;

import com.project.conversion.ConversionApp;

import javax.inject.Singleton;

import dagger.Module;

@Module
@Singleton
public class ApplicationModule {

    private ConversionApp application;

    public ApplicationModule(ConversionApp application) {
        this.application = application;
    }
}
